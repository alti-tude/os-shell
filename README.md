# os-shell

coinfig.c
    has global variables and environment configs

prompt.c
    handles the input prompt

builtins.c
    pwd, echo, cd, remindme, pinfo, clock

ls.c
    implements ls

utilities.c
    initialises the shell

main.c
    controller for everything else